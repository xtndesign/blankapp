import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { AlertController } from "ionic-angular";

import { BarcodeScanner} from "@ionic-native/barcode-scanner";

/**
 * Generated class for the GalleryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html',
})
export class GalleryPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private barcodeScanner: BarcodeScanner,
              private alertController: AlertController) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad GalleryPage');
  }

  scanBarcode() {
    this.barcodeScanner.scan().then((barcodeData) => {
      console.log('test');
    }, (err) => {
        this.presentAlert('Eroare', 'Nu s-a putut scana codul de bare!');
    });
  }

  presentAlert(title, message) {
      let alert = this.alertController.create({
          title: title,
          message: message,
          buttons: ['OK', {
              text: 'Reincearca',
              handler: () => {
                  this.scanBarcode();
              }
          }]
      });

      alert.present();
  }

}
